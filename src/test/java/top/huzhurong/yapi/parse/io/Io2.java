//package top.huzhurong.yapi.parse.io;
//
//import org.junit.jupiter.api.Test;
//
//import java.nio.channels.SelectionKey;
//import java.util.Arrays;
//
///**
// * @author luobo.cs@raycloud.com
// * @module parse
// * @since 2020/12/13 8:21 下午
// */
//public class Io2 {
//
//    @Test
//    void testAndOr() {
//        final int i = 16 & (SelectionKey.OP_READ | SelectionKey.OP_ACCEPT);
//        System.out.println(i);
//        int x = 1000000007;
//        System.out.println(2111 | 1000000007);
//    }
//
//
//    public int breakfastNumber(int[] staple, int[] drinks, int x) {
//        Arrays.sort(staple);
//        Arrays.sort(drinks);
//        int count = 0;
//        int i = 0;
//        int j = drinks.length - 1;
//        for (; i < staple.length && j >= 0; ) {
//            if (staple[i] + drinks[j] > x) {
//                j--;
//            } else {
//                count += (j + 1);
//                count %= 1000000007;
//                i++;
//            }
//        }
//        return count;
//    }
//
//
//}
