package top.huzhurong.yapi.parse.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/11/30 10:21 下午
 */
public class TestIO {

    public static void main(String[] args) throws IOException {
        final Selector selector = Selector.open();
        // socket(domain, type, 0)
        //int arg = 1;
        //setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*)&arg,sizeof(arg))
        //===> setsockopt ==> https://linux.die.net/man/2/setsockopt 系统调用
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //serverSocketChannel.socket() ==> serverSocket的初始化，没有和C交互
        //C 设置setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*)&arg,sizeof(arg))

        serverSocketChannel.configureBlocking(false);
        final ServerSocket serverSocket = serverSocketChannel.socket();
        serverSocket.setReuseAddress(true);
        //
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT, serverSocketChannel);
        //分别执行socket的bind和listener
        serverSocket.bind(new InetSocketAddress("localhost", 9998));
        //将文件描述符配置为非阻塞，在linux中一切皆文件


        //不确定selector在c中是怎么代表的

//        new Thread(() -> {
//
//            for (int i = 0; i < 10; i++) {
//                try {
//                    Thread.sleep(1000_0);
//                    selector.wakeup();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//        }).start();

        System.out.println("-- server ready");
        while (true) {
            final int select = selector.select();
            if (select <= 0) {
                continue;
            }
            final Set<SelectionKey> selectionKeys = selector.selectedKeys();
            for (SelectionKey key : selectionKeys) {
                {
                    if (!key.isValid()) {
                        return;
                    }
                    final ServerSocketChannel ch = (ServerSocketChannel) key.attachment();
                    try {
                        if (key.isAcceptable()) {
                            SocketChannel client_ch = ch.accept();
                            if (client_ch != null) { // accept() may return null...
                                System.out.printf("accepted connection from %s\n", client_ch.getRemoteAddress());
                                client_ch.configureBlocking(false);
                                client_ch.register(selector, SelectionKey.OP_READ, key.attachment());
                            }
                        } else if (key.isReadable()) {
                            System.out.println("11");
                            SocketChannel socketChannel = (SocketChannel) key.channel();
                            socketChannel.register(selector, SelectionKey.OP_WRITE);
                            ByteBuffer buffer = ByteBuffer.allocate(1024);
//                            int read = socketChannel.read(buffer);
                            int read;
                            System.out.println(111);
                            int i = 0;
                            while ((read = socketChannel.read(buffer)) > -1) {
                                buffer.flip();
                                String result = new String(buffer.array(), 0, read).trim();
                                i++;
                                buffer.clear();

                            }

//                            if (read == -1) {
//                                throw new IOException("Socket closed");
//                            }
//                            String result = new String(buffer.array()).trim();
//                            System.out.println("receive:" + result);
//
//                            ByteBuffer writeBuffer = ByteBuffer.wrap(ACK);
//                            Queue<Object> pendingWrites = channelToPendingWrites.get(key.channel());
//                            if (pendingWrites == null) {
//                                synchronized (channelToPendingWrites) {
//                                    pendingWrites = channelToPendingWrites.get(key.channel());
//                                    if (pendingWrites == null) {
//                                        pendingWrites = new ConcurrentLinkedQueue<>();
//                                        channelToPendingWrites.put(key.channel(), pendingWrites);
//                                    }
//                                }
//                            }
//                            pendingWrites.add(writeBuffer);
//                            socketChannel.write(ByteBuffer.wrap(ACK));
//                            Thread.sleep(5000);
                        } else if (key.isWritable()) {
                            System.out.println("就绪====>");
                            SocketChannel socketChannel = (SocketChannel) key.channel();
                            final Queue<Object> objects = channelToPendingWrites.get(socketChannel);
                            if (objects == null || objects.size() == 0) {
                                final ByteBuffer allocate = ByteBuffer.allocate(12);
                                allocate.put("hello,world!".getBytes());
                                socketChannel.write(allocate);
                            } else {
                                final Object poll = objects.poll();
                                socketChannel.write((ByteBuffer) poll);
                            }
                            socketChannel.register(selector, SelectionKey.OP_READ);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static final Map<SelectableChannel, Queue<Object>> channelToPendingWrites = new ConcurrentHashMap<>();

    private static final byte[] ACK = "Data logged successfully\n".getBytes();
}
