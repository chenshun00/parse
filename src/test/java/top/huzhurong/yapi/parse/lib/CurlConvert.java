//package top.huzhurong.yapi.parse.lib;
//
//import top.huzhurong.yapi.parse.http.Request;
//
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author luobo.cs@raycloud.com
// * @module parse
// * @since 2020/11/22 8:29 下午
// */
//public class CurlConvert {
//
//    public static void main(String[] args) throws IOException {
//        final byte[] bytes = Files.readAllBytes(Path.of("/Users/admin/Desktop/curl.txt"));
//        final String curl = new String(bytes, StandardCharsets.UTF_8);
//        final String[] split = curl.split("\n");
//        Request request = new Request();
//        List<Request.Header> headers = new ArrayList<>();
//        request.setHeaders(headers);
//        for (String s : split) {
//            s = s.trim();
//            if (s.endsWith("\\")) {
//                s = s.replace("\\", "");
//            }
//            s = s.trim();
//            if (s.startsWith("curl")) {
//                request.setUrl(s.replace("-X", "")
//                        .replace("POST", "")
//                        .replace("GET", "")
//                        .replace("curl", "")
//                        .replaceAll("'", "").trim());
//            }
//            if (s.startsWith("-H")) {
//                s = s.replaceFirst("-H", "").replaceAll("'", "").trim();
//                final String[] headerSplit = s.split(": ");
//                if (headerSplit.length != 2) {
//                    throw new RuntimeException("解析请求头竟然不是name: value的格式===>" + s);
//                }
//                if (headerSplit[0].trim().equalsIgnoreCase("content-type")) {
//                    final String trim = headerSplit[1].trim();
//                    request.setContentTypeJson(trim.contains("application/json"));
//                    if (request.isContentTypeJson()) {
//                        request.setMethod("POST");
//                    }
//                }
//                Request.Header header = new Request.Header();
//                header.setName(headerSplit[0].trim());
//                header.setValue(headerSplit[1].trim());
//                headers.add(header);
//            }
//            if (s.startsWith("--data-binary") || s.startsWith("--data-raw")) {
//                request.setMethod("POST");
//            } else {
//                request.setMethod("GET");
//            }
//        }
//
//        StringBuilder stringBuilder = new StringBuilder(512);
//        stringBuilder.append("protected static void execHttp() throws IOException {").append("\n\r");
//        stringBuilder.append("    CloseableHttpClient build = HttpClientBuilder.create().build();")
//                .append("\n\r");
//        if (request.getMethod().equalsIgnoreCase("get")) {
//            stringBuilder.append(String.format("    HttpGet httpRequest = new HttpGet(\"%s\");", request.getUrl()))
//                    .append("\n\r");
//        } else {
//            stringBuilder.append(String.format("    HttpPost httpRequest = new HttpPost(\"%s\");", request.getUrl()))
//                    .append("\n\r");
//        }
//        final List<Request.Header> headerList = request.getHeaders();
//        for (Request.Header header : headerList) {
//            stringBuilder.append(String.format("    httpRequest.addHeader(\"%s\", \"%s\");", header.getName(), header.getValue()))
//                    .append("\n\r");
//        }
//        stringBuilder.append("    CloseableHttpResponse execute = build.execute(httpRequest);").append("\n\r");
//        stringBuilder.append("    StatusLine statusLine = execute.getStatusLine();").append("\n\r");
//        stringBuilder.append("    int statusCode = statusLine.getStatusCode();").append("\n\r");
//        stringBuilder.append("    if (statusCode != 200) {").append("\n\r");
//        stringBuilder.append("        throw new IllegalArgumentException(\"请求失败,http响应码不是200:\" + execute);").append("\n\r");
//        stringBuilder.append("    }").append("\n\r");
//        stringBuilder.append("    String responseBody = EntityUtils.toString(execute.getEntity());").append("\n\r");
//        stringBuilder.append("    System.out.println(responseBody);").append("\n\r");
//        stringBuilder.append("}").append("\n\r");
//        System.out.println(stringBuilder.toString());
//
//    }
//
//}
