package top.huzhurong.yapi.parse.netty;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/12/27 7:40 下午
 */
public interface Transport {

    void getConnection();

    void send();
}
