package top.huzhurong.yapi.parse.overTest;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/25 1:30 下午
 */
public class NormalModel implements YapiModel {
    private boolean required;
    private String name;
    private String type;
    private String desc;
    private String defaultValue;

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if (this.desc == null) {
            this.desc = name;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String getModelType() {
        return "nromal";
    }

    @Override
    public String toModelStr() {
        StringBuilder builder = new StringBuilder(128);
        builder.append("\t").append(name).append(required ? "" : "?").append(SEMICOLON).append(SPACE)
                .append(type).append("(").append("description").append("=").append("'").append(desc).append("'").append(")").append(COMMA);
        return builder.toString();
    }
}
