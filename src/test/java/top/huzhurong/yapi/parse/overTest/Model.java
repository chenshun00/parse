package top.huzhurong.yapi.parse.overTest;

import java.util.List;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/25 11:59 上午
 */
public class Model {

    public final static String NEW_LINE = "\n";
    public final static String SPACE = " ";
    public final static String LEFT = "{";
    public final static String RIGHT = "}";
    public final static String COMMA = ",";

    private String modelName;
    private List<YapiModel> modelVars;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public List<YapiModel> getModelVars() {
        return modelVars;
    }

    public void setModelVars(List<YapiModel> modelVars) {
        this.modelVars = modelVars;
    }

    public String toModelStr() {
        StringBuilder builder = new StringBuilder(1024);
        builder.append("model").append(SPACE).append(modelName).append(SPACE).append(LEFT).append(NEW_LINE);

        for (int i = 0; i < modelVars.size(); i++) {

            final YapiModel modelVar = modelVars.get(i);
            builder.append(modelVar.toModelStr());
            if (i != modelVars.size() - 1) {
                builder.append(NEW_LINE);
            }
        }
        builder.append(NEW_LINE).append(RIGHT);
        return builder.toString();
    }

}
