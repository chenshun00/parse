package top.huzhurong.yapi.parse.overTest;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/25 1:30 下午
 */
public interface YapiModel {

    String SEMICOLON = ":";

    String SPACE = " ";

    String NEW_LINE = "\n";
    String LEFT = "{";
    String RIGHT = "}";
    String COMMA = ",";

    String toModelStr();

    default String getModelType() {
        throw new UnsupportedOperationException("不支持的操作!");
    }

}
