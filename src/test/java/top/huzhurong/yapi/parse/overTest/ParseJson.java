package top.huzhurong.yapi.parse.overTest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/25 11:19 上午
 */
public class ParseJson {

    public static String json = "{\"type\":\"object\",\"properties\":{\"age\":{\"type\":\"integer\",\"description\":\"年纪\"},\"name\":{\"type\":\"string\",\"description\":\"姓名\"},\"gg\":{\"type\":\"integer\"},\"Fx\":{\"type\":\"array\",\"items\":{\"type\":\"integer\"}},\"detail\":{\"type\":\"object\",\"properties\":{\"home\":{\"type\":\"string\"},\"father\":{\"type\":\"string\"}},\"required\":[\"home\",\"father\"],\"description\":\"用户详情\"},\"details\":{\"type\":\"array\",\"items\":{\"type\":\"object\",\"properties\":{\"home\":{\"type\":\"string\"},\"father\":{\"type\":\"string\"}},\"required\":[\"home\",\"father\"]},\"description\":\"用户提交详情列表\"},\"action\":{\"type\":\"string\"}},\"required\":[\"age\",\"name\",\"gg\"],\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"description\":\"用户数据 \"}";

    public static void main(String[] args) {
        final JSONObject jsonObject = JSONObject.parseObject(json);

        Model model = new Model();
        model.setModelName("TestRequest");
        model.setModelVars(new ArrayList<>());

        final List<YapiModel> modelVars = model.getModelVars();

        final JSONArray required = jsonObject.getJSONArray("required");

        final JSONObject properties = jsonObject.getJSONObject("properties");
        for (Map.Entry<String, Object> entry : properties.entrySet()) {
            final String key = entry.getKey();
            final JSONObject value = properties.getJSONObject(key);
            final String type = value.getString("type");
            if ("string".equals(type)
                    || "integer".equals(type)
                    || "long".equals(type)
                    || "double".equals(type)
                    || "float".equals(type)) {
                modelVars.addAll(parseNormalModels(key, value, required, type));
            } else if ("object".equals(type)) {
                modelVars.addAll(parseObjectModels(key, value, jsonObject.getJSONArray("required"), required.contains(key)));
            } else if ("array".equals(type)) {
                modelVars.addAll(parseArrayModels(key, value, required));
            } else {
                throw new IllegalStateException(String.format("未知类型:%s", type));
            }
        }
        System.out.println(JSONObject.toJSONString(model));
        System.out.println("=============================");
        System.out.println(model.toModelStr());
    }

    public static List<YapiModel> parseArrayModels(String key, JSONObject value, JSONArray required) {
        List<YapiModel> yapiModels = new ArrayList<>();
        //可能是一个对象List
        //可能是一个字符串list | 任何一个基本类型的list都是可能的
        ArrayModel arrayModel = new ArrayModel();
        arrayModel.setNormalModels(new ArrayList<>());

        final JSONObject items = value.getJSONObject("items");
        final String type = items.getString("type");
        if ("object".equals(type)) {
            final List<YapiModel> tempModels = parseObjectModels(key, items, items.getJSONArray("required"), required.contains(key));
            arrayModel.getNormalModels().addAll(((ObjectModel) tempModels.get(0)).getNormalModels());
        } else if ("string".equals(type)
                || "integer".equals(type)
                || "long".equals(type)
                || "double".equals(type)
                || "float".equals(type)) {
            BasicArray basicArray = new BasicArray();
            basicArray.setName(key);
            basicArray.setRequired(required.contains(key));
            basicArray.setDesc(key);
            basicArray.setType(type);
            return Collections.singletonList(basicArray);
        } else {
            throw new IllegalStateException("需要额外的添加判断条件了:" + type);
        }

        arrayModel.setName(key);
        arrayModel.setDesc(key);
        arrayModel.setRequired(required.contains(key));
        yapiModels.add(arrayModel);
        return yapiModels;
    }

    public static List<YapiModel> parseObjectModels(String key, JSONObject value, JSONArray required, boolean require) {
        if (required == null) {
            required = new JSONArray();
        }
        List<YapiModel> yapiModels = new ArrayList<>();
        ObjectModel objectModel = new ObjectModel();
        objectModel.setRequired(require);
        objectModel.setName(key);
        objectModel.setDesc(key);
        objectModel.setNormalModels(new ArrayList<>());
        JSONObject properties = value.getJSONObject("properties");
        if (properties == null) {
            properties = new JSONObject();
        }
        for (Map.Entry<String, Object> entry : properties.entrySet()) {
            final String name = entry.getKey();
            final JSONObject detail = properties.getJSONObject(name);
            final String type = detail.getString("type");
            if ("string".equals(type)
                    || "integer".equals(type)
                    || "long".equals(type)
                    || "double".equals(type)
                    || "float".equals(type)) {
                objectModel.getNormalModels().addAll(parseNormalModels(name, value, required, type));
            } else if ("object".equals(type)) {
                objectModel.getNormalModels().addAll(parseObjectModels(key, value,
                        detail.getJSONArray("required"), required.contains(key)));
            } else if ("array".equals(type)) {
                objectModel.getNormalModels().addAll(parseArrayModels(key, value, required));
            } else {
                throw new IllegalStateException(String.format("未知类型:%s", type));
            }
        }

        yapiModels.add(objectModel);
        return yapiModels;
    }

    public static List<YapiModel> parseNormalModels(String key, JSONObject value, JSONArray required, String type) {
        List<YapiModel> yapiModels = new ArrayList<>();
        final String description = value.getString("description");
        NormalModel modelVar = new NormalModel();
        modelVar.setDefaultValue(null);
        modelVar.setDesc(description);
        modelVar.setType(type);
        modelVar.setName(key);
        modelVar.setRequired(required.contains(key));
        yapiModels.add(modelVar);
        return yapiModels;
    }

}
