package top.huzhurong.yapi.parse.overTest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/25 12:31 下午
 */
public class ThreadTest {

    final Logger log = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) throws InterruptedException {

        final Object o = new Object();
        o.wait();

//        final Object xx1 = new Object();
//        final Object xx2 = new Object();
//        final Object xx3 = new Object();
//        Thread a = new Thread(() -> {
//
//            try {
//                synchronized (xx1) {
//                    xx1.wait();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            int a1 = 0;
//            while (a1 < 100) {
//                System.out.print("a");
//                a1++;
//
//                synchronized (xx2) {
//                    xx2.notify();
//                }
//                try {
//                    synchronized (xx1) {
//                        xx1.wait();
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        a.setDaemon(true);
//        a.setName("aaa");
//        Thread b = new Thread(() -> {
//            try {
//                synchronized (xx2) {
//                    xx2.wait();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            int b1 = 0;
//            while (b1 < 100) {
//                System.out.print("b");
//                b1++;
//                synchronized (xx3) {
//                    xx3.notify();
//                }
//                try {
//                    synchronized (xx2) {
//                        xx2.wait();
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        b.setDaemon(true);
//        b.setName("bbb");
//        Thread c = new Thread(() -> {
//            try {
//                synchronized (xx3) {
//                    xx3.wait();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            int c1 = 0;
//            while (c1 < 100) {
//                System.out.println("c");
//                c1++;
//                synchronized (xx1) {
//                    xx1.notify();
//                }
//                try {
//                    synchronized (xx3) {
//                        xx3.wait();
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        c.setDaemon(true);
//        c.setName("ccc");
//        a.start();
//        b.start();
//        c.start();
//
//        synchronized (xx1) {
//            xx1.notify();
//        }
//
//        try {
//            Thread.sleep(100L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

}
