package top.huzhurong.yapi.parse.overTest;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/25 3:14 下午
 */
public class BasicArray implements YapiModel {

    private String name;
    private boolean required;
    private String desc;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getModelType() {
        return "basicArray";
    }

    @Override
    public String toModelStr() {
        StringBuilder builder = new StringBuilder();
        builder.append("\t").append(name).append(required ? "" : "?").append(SEMICOLON).append(SPACE)
                .append("[ ").append(type).append(" ]")
                .append("(").append("description").append("=").append("'")
                .append(desc).append("'").append(")").append(COMMA);
        return builder.toString();
    }
}
