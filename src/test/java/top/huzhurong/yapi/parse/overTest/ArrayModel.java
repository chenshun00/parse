package top.huzhurong.yapi.parse.overTest;

import java.util.List;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/25 1:32 下午
 */
public class ArrayModel implements YapiModel {

    private String name;
    private boolean required;
    private String desc;
    private List<YapiModel> normalModels;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<YapiModel> getNormalModels() {
        return normalModels;
    }

    public void setNormalModels(List<YapiModel> normalModels) {
        this.normalModels = normalModels;
    }

    @Override
    public String getModelType() {
        return "array";
    }

    @Override
    public String toModelStr() {
        StringBuilder builder = new StringBuilder(128);
        builder.append("\t").append(name).append(required ? "" : "?").append(SEMICOLON)
                .append("[").append(LEFT).append(NEW_LINE);
        for (int i = 0; i < normalModels.size(); i++) {
            builder.append(normalModels.get(i).toModelStr());
            if (i != normalModels.size() - 1) {
                builder.append(NEW_LINE);
            }
        }
        builder.append(NEW_LINE).append("\t").append("}").append("]")
                .append("(").append("description").append("=").append("'")
                .append(desc).append("'").append(")").append(COMMA);
        return builder.toString();
    }
}
