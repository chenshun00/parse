package top.huzhurong.yapi.parse.TT;

import com.alibaba.csp.sentinel.concurrent.NamedThreadFactory;
import com.alibaba.csp.sentinel.transport.command.http.HttpEventTask;
import com.alibaba.csp.sentinel.transport.log.CommandCenterLog;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/12/25 5:50 下午
 */
public class Xf {

    private static volatile ExecutorService bizExecutor = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new ArrayBlockingQueue(10), new NamedThreadFactory("sentinel-command-center-service-executor"), new RejectedExecutionHandler() {
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            CommandCenterLog.info("EventTask rejected", new Object[0]);
            throw new RejectedExecutionException();
        }
    });


    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newSingleThreadExecutor(
                new NamedThreadFactory("sentinel-command-center-executor"));


        new Thread(() -> {
            ServerSocket serverSocket = null;
            try {
                serverSocket = new ServerSocket(9898);
                CommandCenterLog.info("[CommandCenter] Begin listening at port " + serverSocket.getLocalPort());
                executor.submit(new ServerThread(serverSocket));
                System.out.println("fffff");
                executor.shutdown();
                System.out.println("xxx===>" + executor.isShutdown());
                System.out.println("xxx===>" + executor.isTerminated());
                Thread.sleep(10000);
                System.out.println("这里");
                serverSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }).start();

        new Thread(() -> {
            while (!executor.isTerminated()) {
                try {
                    System.out.println("ggggg1");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        Thread.sleep(3000L);
        System.out.println("结束");
    }

    static class ServerThread extends Thread {
        private ServerSocket serverSocket;

        ServerThread(ServerSocket s) {
            this.serverSocket = s;
            this.setName("sentinel-courier-server-accept-thread");
        }

        public void run() {
            while (true) {
                Socket socket = null;

                try {
                    socket = this.serverSocket.accept();
                    socket.setSoTimeout(5000);
                    HttpEventTask eventTask = new HttpEventTask(socket);
                    bizExecutor.submit(eventTask);
                } catch (Exception var6) {
                    CommandCenterLog.info("Server error", var6);
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (Exception var5) {
                            CommandCenterLog.info("Error when closing an opened socket", var5);
                        }
                    }

                    try {
                        Thread.sleep(10L);
                    } catch (InterruptedException var4) {
                        return;
                    }
                }
            }
        }
    }
}
