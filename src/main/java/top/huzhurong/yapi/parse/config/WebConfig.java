package top.huzhurong.yapi.parse.config;

import com.alibaba.csp.sentinel.adapter.servlet.CommonFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.huzhurong.yapi.parse.dubbo.ServiceParameterBeanPostProcessor;

import javax.servlet.Filter;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/12/25 3:46 下午
 */
@Configuration
public class WebConfig {
    @Bean
    public FilterRegistrationBean<Filter> sentinelFilterRegistration() {
        FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new CommonFilter());
        registration.addUrlPatterns("/*");
        registration.setName("sentinelFilter");
        registration.setOrder(1);

        return registration;
    }

    @Bean
    public ServiceParameterBeanPostProcessor serviceParameterBeanPostProcessor() {
        return new ServiceParameterBeanPostProcessor();
    }

}
