package top.huzhurong.yapi.parse.http;

import java.util.List;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/11/22 8:33 下午
 */
public class Request {

    private String url;
    private List<Header> headers;
    private String method;

    private boolean contentTypeJson;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Header> getHeaders() {
        return headers;
    }

    public void setHeaders(List<Header> headers) {
        this.headers = headers;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean isContentTypeJson() {
        return contentTypeJson;
    }

    public void setContentTypeJson(boolean contentTypeJson) {
        this.contentTypeJson = contentTypeJson;
    }

    public static class Header {
        private String name;
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
