package top.huzhurong.yapi.parse.order;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/12/2 1:40 下午
 */
public interface OrderHandlerService {

    void handle(OrderContext orderContext);

}
