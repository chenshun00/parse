package top.huzhurong.yapi.parse.order;

import top.huzhurong.yapi.parse.annotation.Fuck;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/12/2 1:27 下午
 */
public abstract class BaseOrder {

    public abstract void handleOrder(OrderContext orderContext);

    public boolean support(String orderType) {
        final Fuck fuck = this.getClass().getAnnotation(Fuck.class);
        return Arrays.stream(fuck.value()).collect(Collectors.toSet()).contains(orderType);
    }

    public abstract Fuck getSupportedOrderType();
}
