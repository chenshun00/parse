package top.huzhurong.yapi.parse.order;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/12/2 1:46 下午
 */
public class OrderContext {

    private Object order;

    private String orderType;

    public Object getOrder() {
        return order;
    }

    public void setOrder(Object order) {
        this.order = order;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}
