package top.huzhurong.yapi.parse.order.handle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import top.huzhurong.yapi.parse.annotation.Fuck;
import top.huzhurong.yapi.parse.order.BaseOrder;
import top.huzhurong.yapi.parse.order.OrderContext;
import top.huzhurong.yapi.parse.order.OrderHandlerService;
import top.huzhurong.yapi.parse.order.exception.OrderRejectException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/12/2 1:31 下午
 */
@Component
public class OrderHandler implements OrderHandlerService, InitializingBean, BeanFactoryAware {

    private final List<BaseOrder> orderList = new ArrayList<>();

    private BeanFactory beanFactory;

    @Override
    public void handle(OrderContext orderContext) {
        final BaseOrder baseOrder = orderList.stream()
                .filter(x -> x.support(orderContext.getOrderType()))
                .findAny()
                .orElseThrow(() -> new OrderRejectException("不支持的订单类型"));
        baseOrder.handleOrder(orderContext);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(beanFactory, "beanFactory can't be null");
        final String[] beanNames = BeanFactoryUtils.beanNamesForAnnotationIncludingAncestors((ListableBeanFactory) beanFactory, Fuck.class);
        for (String beanName : beanNames) {
            orderList.add((BaseOrder) beanFactory.getBean(beanName));
        }
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
