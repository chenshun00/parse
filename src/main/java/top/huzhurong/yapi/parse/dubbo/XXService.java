package top.huzhurong.yapi.parse.dubbo;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import top.huzhurong.best.bootmybatis.zz.TTT;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2021/1/4 4:57 下午
 */
@Service
public class XXService implements  InitializingBean {

    @Reference(timeout = 1000, version = "1.0.0")
    private TTT ttt;

    @Override
    public void afterPropertiesSet() {
        new Thread(() -> {
            while (true) {
                try {
//                    final String xx = ttt.xx();
                    System.out.println(ttt.xx());
                } catch (Exception e) {
                    System.out.println("发生一场场:" + e.getMessage());
                }
                try {
                    Thread.sleep(10000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
