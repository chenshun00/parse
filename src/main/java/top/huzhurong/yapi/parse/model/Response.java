package top.huzhurong.yapi.parse.model;

import javax.validation.constraints.NotNull;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/11/15 2:09 下午
 */
public class Response<T> {

    /**
     * 响应码 100成功 非100失败
     */
    @NotNull
    private Integer code = 100;

    /**
     * 业务失败的message，例如:xx参数为空
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
