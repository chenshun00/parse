package top.huzhurong.yapi.parse.model.req;

import org.hibernate.validator.constraints.Length;
import top.huzhurong.yapi.parse.model.BaseRequest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/11/15 2:10 下午
 */
public class UserModelRequest extends BaseRequest {

    /**
     * 年纪
     */
    @NotNull(message = "年纪不能为空")
    private Integer age;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @Length(min = 3, message = "姓名3位以上")
    private String name;

    @NotBlank(message = "FF不能为空")
    private Long gg;

    @NotBlank(message = "FF不能为空")
    private Double doublez;

    @NotBlank(message = "FF不能为空")
    private Float aFloat;

    List<Integer> Fx;

    /**
     * 用户详情
     */
    private Detail detail;

    /**
     * 用户提交详情列表
     */
    private List<Detail> details;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public Long getGg() {
        return gg;
    }

    public Double getDoublez() {
        return doublez;
    }

    public void setDoublez(Double doublez) {
        this.doublez = doublez;
    }

    public Float getaFloat() {
        return aFloat;
    }

    public void setaFloat(Float aFloat) {
        this.aFloat = aFloat;
    }

    public void setGg(Long gg) {
        this.gg = gg;
    }

    public List<Integer> getFx() {
        return Fx;
    }

    public void setFx(List<Integer> fx) {
        Fx = fx;
    }

    public static class Detail {
        @NotBlank
        private String home;

        @NotBlank
        private String father;

        public String getHome() {
            return home;
        }

        public void setHome(String home) {
            this.home = home;
        }

        public String getFather() {
            return father;
        }

        public void setFather(String father) {
            this.father = father;
        }
    }
}
