package top.huzhurong.yapi.parse.model.resp;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/11/15 2:10 下午
 */
public class UserModelResponse {

    /**
     * 是否婚配
     */
    private boolean merge = false;

    /**
     * 年纪
     */
    @NotNull(message = "年纪不能为空")
    private Integer age;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @Length(min = 3, message = "姓名3位以上")
    private String name;

    /**
     * 用户详情数据
     */
    @NotNull
    private Detail detail;

    /**
     * 详情列表
     */
    @NotNull
    private List<Detail> details;

    public boolean isMerge() {
        return merge;
    }

    public void setMerge(boolean merge) {
        this.merge = merge;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public static class Detail {
        @NotBlank
        private String home;

        @NotBlank
        private String father;

        public String getHome() {
            return home;
        }

        public void setHome(String home) {
            this.home = home;
        }

        public String getFather() {
            return father;
        }

        public void setFather(String father) {
            this.father = father;
        }
    }
}
