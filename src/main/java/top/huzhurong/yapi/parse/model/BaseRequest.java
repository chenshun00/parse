package top.huzhurong.yapi.parse.model;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/11/20 9:20 上午
 */
public abstract class BaseRequest {

    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
