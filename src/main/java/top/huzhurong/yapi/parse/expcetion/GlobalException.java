package top.huzhurong.yapi.parse.expcetion;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.HandlerMethod;
import top.huzhurong.yapi.parse.model.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author luobo.cs@raycloud.com
 * @module parse
 * @since 2020/11/15 1:57 下午
 */
@RestControllerAdvice
public class GlobalException {

    public static final Logger logger = LoggerFactory.getLogger(GlobalException.class);

    @ExceptionHandler(BindException.class)
    public Object handle(BindException bindException) {
        final FieldError fieldError = bindException.getFieldError();
        String message = "参数{".concat(fieldError.getField()).concat("}").concat(fieldError.getDefaultMessage());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error", message);
        return jsonObject;
    }

    @ExceptionHandler(Exception.class)
    public Response<Void> common(Exception exception, HttpServletRequest req, HttpServletResponse resp, HandlerMethod handle) {
        Response<Void> error = new Response<>();
        error.setCode(200);
        error.setMessage(exception.getMessage());
        return error;
    }

//
//    /**
//     * Validator 参数校验异常处理
//     *
//     * @param ex
//     * @return
//     */
//    @ExceptionHandler(value = BindException.class)
//    public ResponseEntity<Object> handleMethodVoArgumentNotValidException(BindException ex) {
//        FieldError err = ex.getFieldError();
//        // err.getField() 读取参数字段
//        // err.getDefaultMessage() 读取验证注解中的message值
//        String message = "参数{".concat(err.getField()).concat("}").concat(err.getDefaultMessage());
//        logger.info("{} -> {}", err.getObjectName(), message);
//        return new ResponseEntity<Object>(new ResultVO(ResultCode.PARAM_INVALID, message), HttpStatus.OK);
//    }

//    /**
//     * Validator 参数校验异常处理
//     *
//     * @param ex
//     * @return
//     */
//    @ExceptionHandler(value = ConstraintViolationException.class)
//    public ResponseEntity<Object> handleMethodArgumentNotValidException(ConstraintViolationException ex) {
//        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
//        for (ConstraintViolation<?> constraintViolation : constraintViolations) {
//            PathImpl pathImpl = (PathImpl) constraintViolation.getPropertyPath();
//            // 读取参数字段，constraintViolation.getMessage() 读取验证注解中的message值
//            String paramName = pathImpl.getLeafNode().getName();
//            String message = "参数{".concat(paramName).concat("}").concat(constraintViolation.getMessage());
//            logger.info("{} -> {} -> {}", constraintViolation.getRootBeanClass().getName(), pathImpl.toString(), message);
//            return new ResponseEntity<Object>(new ResultVO(ResultCode.PARAM_INVALID, message), HttpStatus.OK);
//        }
//        return new ResponseEntity<Object>(new ResultVO(ResultCode.PARAM_INVALID, ex.getMessage()), HttpStatus.OK);
//    }
}
