package top.huzhurong.yapi.parse.view;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import top.huzhurong.yapi.parse.model.Response;
import top.huzhurong.yapi.parse.model.req.UserModelRequest;
import top.huzhurong.yapi.parse.model.resp.UserModelResponse;

/**
 * yapi测试
 *
 * @author luobo.cs@raycloud.com
 * @module yapi测试
 * @since 2020/11/15 1:45 下午
 */
@RestController
@RequestMapping("v1")
public class TestCtrl {

    final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 测试POST body生成的数据
     *
     * @param userModelRequest 用户数据
     * @return 用户登录详情
     */
    @PostMapping("test1")
    public Response<UserModelResponse> test1(@RequestBody UserModelRequest userModelRequest) {
        System.out.println(JSONObject.toJSONString(userModelRequest));
        Response<UserModelResponse> result = new Response<>();
        UserModelResponse userModelResponse = new UserModelResponse();
        userModelResponse.setAge(200);
        userModelResponse.setName("chenshun");
        result.setData(userModelResponse);
        return result;
    }

    /**
     * 测试POST 表单生成的数据
     *
     * @param userModelRequest 用户数据
     * @return 用户登录详情
     */
    @PostMapping("test2")
    public Response<UserModelResponse> test2(UserModelRequest userModelRequest) {
        System.out.println(JSONObject.toJSONString(userModelRequest));
        Response<UserModelResponse> result = new Response<>();
        UserModelResponse userModelResponse = new UserModelResponse();
        userModelResponse.setAge(200);
        userModelResponse.setName("chenshun");
        result.setData(userModelResponse);
        return result;
    }

    /**
     * 测试GET 表单生成的数据
     *
     * @param userModelRequest 用户数据
     * @return 用户登录详情
     */
    @GetMapping("test3")
    public Response<UserModelResponse> test3(UserModelRequest userModelRequest) {
        System.out.println(JSONObject.toJSONString(userModelRequest));
        Response<UserModelResponse> result = new Response<>();
        UserModelResponse userModelResponse = new UserModelResponse();
        userModelResponse.setAge(200);
        userModelResponse.setName("chenshun");
        result.setData(userModelResponse);
        return result;
    }

}
